import React from 'react';
import { Row, Form, Col, Button } from 'react-bootstrap';

class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      id: '',
      title: '',
      price: '',
      category: '',
      isTitleError: '',
      isPriceError: '',
      isCategoryError: ''
    }

    if (props.product) {
      this.state = props.product
    } else {
      this.state = this.initialState;
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.isValid()) {
      this.setState({ 'error': "" })
      this.props.onFormSubmit(this.state);
      this.setState(this.initialState);
    }
  }
  isValid() {
    let isValid = false;
    this.setState({ isTitleError: "", isPriceError: "", isCategoryError: "" })
    if (!this.state.title) {
      this.setState({ isTitleError: "Please Enter Product Name" })
      return false
    }
    if (!this.state.price) {
      this.setState({ isPriceError: "Please Enter Product Price" })
      return false
    }
    if (!this.state.category) {
      this.setState({ isCategoryError: "Please Enter Product Category" })
      return false
    }
    
    return true
  }

  render() {

    let pageTitle;
    if (this.state.id) {
      pageTitle = <h2>Edit Product</h2>
    } else {
      pageTitle = <h2>Add Product</h2>
    }

    return (
      <div>
        {pageTitle}
        <Row>
          <Col sm={6}>
            <Form onSubmit={this.handleSubmit}>
              <Form.Group controlId="productName">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  value={this.state.title}
                  onChange={this.handleChange}
                  placeholder="Product Name"
                  isInvalid={!!this.state.isTitleError} />
                <Form.Control.Feedback type="invalid">
                  {this.state.isTitleError}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  value={this.state.price}
                  onChange={this.handleChange}
                  placeholder="Price"
                  isInvalid={!!this.state.isPriceError} />
                <Form.Control.Feedback type="invalid">
                  {this.state.isPriceError}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="category">
                <Form.Label>Category</Form.Label>
                <Form.Control
                  type="text"
                  name="category"
                  value={this.state.category}
                  onChange={this.handleChange}
                  placeholder="Category"
                  isInvalid={!!this.state.isCategoryError} />
                <Form.Control.Feedback type="invalid">
                  {this.state.isCategoryError}
                </Form.Control.Feedback>
              </Form.Group>
              {/* {this.state.error && (<Form.Control.Feedback type="invalid">
                {this.state.error}
              </Form.Control.Feedback>)} */}
              <Form.Group>
                <Form.Control type="hidden" name="id" value={this.state.id} />
                <Button type="submit">Save</Button>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}

export default AddProduct;
