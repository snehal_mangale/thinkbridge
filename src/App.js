import React, { Component } from 'react';
import './App.css';
import { Container, Button, Alert } from 'react-bootstrap';
import ProductList from '../src/components/productList';
import AddProduct from '../src/components/addProduct';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddProduct: false,
      error: null,
      response: {},
      product: {},
      products: null,
      isEditProduct: false,
      isDeleteProduct: false
    }
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }
  componentDidCatch(){
    this.setState({error:"Caught some error during render"})
  }
  componentDidMount() {
    const apiUrl = 'https://fakestoreapi.com/products';

    fetch(apiUrl)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            products: result
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }

  onCreate() {
    this.setState({ isAddProduct: true, product: null,isDeleteProduct:false });
  }

  onFormSubmit(data) {
    let apiUrl;
    let method = '';
    if (this.state.isEditProduct) {
      apiUrl = `https://fakestoreapi.com/products/${data.id}`;
      method = 'PUT'
    } else {
      apiUrl = 'https://fakestoreapi.com/products';
      method = 'POST'
    }

    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    const options = {
      method: method,
      body: JSON.stringify(data),
      myHeaders
    };

    fetch(apiUrl, options)
      .then(res => res.json())
      .then(result => {
        if (!this.state.isEditProduct) {
          var product = {
            id: this.state.products.length + 1,
            title: data.title,
            price: data.price,
            category: data.category
          }
          let products = this.state.products;
          products.push(product)
          this.setState({
            products: products,
            response: data,
            isAddProduct: false,
            isEditProduct: false,
            isDeleteProduct: false
          })
        } else {
          var product = {
            id: data.id,
            title: data.title,
            price: data.price,
            category: data.category
          }
          let products = this.state.products;
          products.map((productData, index) => {
            if (productData.id === parseInt(product.id)) {
              products[index] = product;
            }
          })
          this.setState({
            response: data,
            products: products,
            isAddProduct: false,
            isEditProduct: false,
            isDeleteProduct: false
          })
        }
      },
        (error) => {
          this.setState({ error });
        }
      )
  }

  editProduct = product => {
    const apiUrl = `https://fakestoreapi.com/products/${product.id}`;
    this.setState({isDeleteProduct:false})
    const formData = new FormData();
    formData.append('productId', product);
    //  let product = this.state.response.filter(product => product.id === productId)
    const options = {
      method: 'PUT',
      body: JSON.stringify(formData)
    }

    fetch(apiUrl, options)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            product: product,
            isEditProduct: true,
            isAddProduct: true,
            isDeleteProduct: false
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }

  deleteProduct = (productId) => {
    const apiUrl = `https://fakestoreapi.com/products/${productId}`;
    const options = {
      method: 'DELETE',
      body: productId
    }

    fetch(apiUrl, options)
      .then(res => res.json())
      .then(
        (result) => {
          var prods = this.state.products;
          prods.map((productObj, index) => {
            if (productObj.id === productId) {
              prods.splice(index, 1)
            }
          })
          this.setState({
            products: prods,
            isAddProduct: false,
            isDeleteProduct: true
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }

  render() {

    let productForm;
    if ((this.state.isAddProduct || this.state.isEditProduct)) {
      productForm = <AddProduct onFormSubmit={this.onFormSubmit} product={this.state.product} />
    }
    let isNoAddTable;
    if (!this.state.isDeleteProduct && !this.state.isAddProduct && !this.state.isEditProduct) {
      isNoAddTable = <ProductList products={this.state.products}
        editProduct={this.editProduct} deleteProduct={this.deleteProduct} />
    }
    let deleteTable
    if (this.state.isDeleteProduct) {
      deleteTable = <ProductList products={this.state.products}
        editProduct={this.editProduct} deleteProduct={this.deleteProduct} />
    }

    return (
      <div className="App">
        <Container>
          {!this.state.isAddProduct && <Button variant="primary" onClick={() => this.onCreate()}>Add Product</Button>}
          {this.state.response.status === 'success' && <div><br /><Alert variant="info">{this.state.response.message}</Alert></div>}
          {/* { (this.state.isDeleteProduct || !this.state.isAddProduct )   && 
                  (<ProductList products={this.state.products}
                    editProduct={this.editProduct} deleteProduct={this.deleteProduct}/>
                 )}    */}
          {!this.state.isAddProduct && isNoAddTable}
          {deleteTable}
          {productForm}
          {this.state.error && <div>Error: {this.state.error.message}</div>}
        </Container>
      </div>
    );
  }
}

export default App;

